package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	_ "github.com/gin-gonic/gin"

	"github.com/stretchr/testify/assert"
)

func TestPingRoute(t *testing.T) {
	router := createRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.JSONEq(t, "{\"message\": \"pong\"}", w.Body.String())
}
