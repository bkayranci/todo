package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/bkayranci/todo/controllers"
	"gitlab.com/bkayranci/todo/models"
)

func createRouter() *gin.Engine {
	router := gin.Default()
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowCredentials = true

	router.Use(cors.New(config))

	v1 := router.Group("/v1")
	{
		todo := new(controllers.ToDoController)

		v1.GET("/todos", todo.All)
		v1.POST("/todos", todo.Create)
		v1.GET("/todos/:id", todo.One)
		v1.PUT("/todos/:id", todo.Update)
		v1.DELETE("/todos/:id", todo.Delete)
	}

	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	return router
}

func main() {
	router := createRouter()
	models.ConnectDatabase()
	if err := router.Run(); err != nil {
		panic("Application not run!")
	}
}
