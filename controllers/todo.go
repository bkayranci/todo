package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/bkayranci/todo/models"
)

type ToDoController struct{}

type CreateToDoInput struct {
	Title   string `json:"Title" binding:"required"`
	Content string `json:"Content" binding:"required"`
}

type UpdateToDoInput struct {
	Title   string `json:"Title"`
	Content string `json:"Content"`
}

func (ctrl ToDoController) All(c *gin.Context) {
	var todos []models.ToDo
	models.DB.Find(&todos)

	c.JSON(http.StatusOK, todos)
}

func (ctrl ToDoController) One(c *gin.Context) {

	var todo models.ToDo
	if err := models.DB.Where("id = ?", c.Param("id")).First(&todo).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, todo)
}

func (ctrl ToDoController) Create(c *gin.Context) {
	var input CreateToDoInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
		return
	}

	todo := models.ToDo{Title: input.Title, Content: input.Content}
	models.DB.Create(&todo)

	c.JSON(http.StatusCreated, todo)
}

func (ctrl ToDoController) Update(c *gin.Context) {
	var todo models.ToDo
	if err := models.DB.Where("id = ?", c.Param("id")).First(&todo).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Record not found!"})
		return
	}

	var input UpdateToDoInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	models.DB.Model(&todo).Updates(models.ToDo{Title: input.Title, Content: input.Content})

	c.JSON(http.StatusOK, todo)
}

func (ctrl ToDoController) Delete(c *gin.Context) {
	var todo models.ToDo
	if err := models.DB.Where("id = ?", c.Param("id")).First(&todo).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&todo)

	c.JSON(http.StatusNoContent, gin.H{"data": true})
}
