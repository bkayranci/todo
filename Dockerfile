FROM golang:1.16-alpine AS builder

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
    GIN_MODE=release

WORKDIR /build

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .

RUN go build -o todo .
RUN chmod 700 /build/todo
RUN chown nobody:nobody /build/todo

FROM scratch as runtime
COPY --from=builder /build/todo /
COPY --from=builder /etc/passwd /etc/passwd

USER nobody
EXPOSE 8080
ENTRYPOINT [ "/todo" ]
