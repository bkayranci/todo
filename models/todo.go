package models

import (
	"gorm.io/gorm"
)

type ToDo struct {
	gorm.Model
	Title   string `json:"Title"      gorm:"type:varchar(255)"`
	Content string `json:"Content"    gorm:"type:text"`
}
