package models

import (
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnectDatabase() {
	uri := os.Getenv("DB_URI")
	database, err := gorm.Open(postgres.Open(uri), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

	if err := database.AutoMigrate(&ToDo{}); err != nil {
		panic("Fail to migrate")
	}

	DB = database
}
