module gitlab.com/bkayranci/todo

go 1.16

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.2
	github.com/stretchr/testify v1.5.1
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.11
)
